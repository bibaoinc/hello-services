package com.bibao.aws.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bibao.aws.app.HelloApp;

@SpringBootTest(classes = HelloApp.class, webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloControllerTest {
	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@BeforeEach
	public void setup() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
	@Test
	public void testSayHello() throws Exception {
		String requestUrl = "/Bob";
		
		ResultActions resultActions = mvc.perform(MockMvcRequestBuilders
										.get(requestUrl)
										.contentType(MediaType.APPLICATION_JSON)
										.accept(MediaType.APPLICATION_JSON))
										.andExpect(MockMvcResultMatchers.status().isOk());
		
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Hello Bob, welcome to AWS!"));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$.successful").value(true));
	}
}
