package com.bibao.aws.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.aws.config.HelloConfig;

@SpringBootApplication
@Import({HelloConfig.class})
public class HelloApp {
	public static void main(String[] args) {
		SpringApplication.run(HelloApp.class, args);
	}
}
