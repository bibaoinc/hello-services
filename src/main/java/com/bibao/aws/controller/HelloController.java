package com.bibao.aws.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.aws.model.HelloResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@Api(value = "Hello Services")
public class HelloController {
	private static final Logger LOG = LoggerFactory.getLogger(HelloController.class);
	
	@GetMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "Say Hello", response = HelloResponse.class)
	@ApiResponses(value = {
    		@ApiResponse(code = 200, response = HelloResponse.class, message = "Successful response"),
    		@ApiResponse(code = 400, message = "Bad request")})
	public ResponseEntity<HelloResponse> sayHello(@PathVariable("name") String name) {
		LOG.debug("Received a request to say hello to {}", name);
		HelloResponse response = new HelloResponse();
		response.setMessage("Hello " + name + ", welcome to AWS!");
		response.setSuccessful(true);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
